// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:newsapi/src/models/article.dart';

import '../api/newsapi_api.dart';

abstract class ITechcrunchTrendingRepository {
  final NewsapiApi newsapiApi;

  ITechcrunchTrendingRepository(this.newsapiApi);

  Future<List<Article>> getTrendingStories();

  void dispose();
}

class TechCrunchTrendingRepository extends ITechcrunchTrendingRepository {
  TechCrunchTrendingRepository(NewsapiApi newsapiApi) : super(newsapiApi);

  @override
  Future<List<Article>> getTrendingStories() {
    return newsapiApi.getTechCrunchTrendingStories();
  }

  @override
  void dispose() {
    newsapiApi.dispose();
  }
}
