// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:newsapi/src/models/article.dart';
import 'package:newsapi/src/models/articles.dart';

abstract class INewsapiApi {
  final http.Client client;
  final String baseUrl;
  final String apiKey;

  INewsapiApi(
    this.client,
    this.baseUrl,
    this.apiKey,
  );

  Future<List<Article>> getTechCrunchTrendingStories();

  void dispose() {}
}

class NewsapiApi extends INewsapiApi {
  NewsapiApi(
    http.Client client,
    String baseUrl,
    String apiKey,
  ) : super(
          client,
          baseUrl,
          apiKey,
        );

  @override
  Future<List<Article>> getTechCrunchTrendingStories() async {
    var response = await client
        .get('$baseUrl/top-headlines?sources=techcrunch&apiKey=$apiKey');

    if (response.statusCode == 200) {
      final articles = Articles.fromJson(json.decode(response.body));
      return articles.articles;
    } else if (response.statusCode == 999) {
      // TODO: Add other necessary status codes here depending on the API you are consuming.
      throw UnimplementedError("Add implementation here!");
    } else {
      throw Exception('Failed to tech crunch trending stories');
    }
  }
}
