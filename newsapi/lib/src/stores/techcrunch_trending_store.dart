// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:mobx/mobx.dart';
import 'package:newsapi/newsapi.dart';
import 'package:newsapi/src/models/article.dart';

part 'techcrunch_trending_store.g.dart';

class TechCrunchTrendingStore = _TechCrunchTrendingStore
    with _$TechCrunchTrendingStore;

abstract class _TechCrunchTrendingStore with Store {
  _TechCrunchTrendingStore(this.techCrunchTrendingRepository);
  final TechCrunchTrendingRepository techCrunchTrendingRepository;

  @observable
  List<Article> trendingStories = [];

  @action
  getTrendingRepositories() async {
    // TODO: Add support for pagination
    trendingStories = await techCrunchTrendingRepository.getTrendingStories();
  }

  void dispose() {
    techCrunchTrendingRepository.dispose();
  }
}
