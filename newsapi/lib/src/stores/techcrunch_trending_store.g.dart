// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'techcrunch_trending_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TechCrunchTrendingStore on _TechCrunchTrendingStore, Store {
  final _$trendingStoriesAtom =
      Atom(name: '_TechCrunchTrendingStore.trendingStories');

  @override
  List<Article> get trendingStories {
    _$trendingStoriesAtom.context.enforceReadPolicy(_$trendingStoriesAtom);
    _$trendingStoriesAtom.reportObserved();
    return super.trendingStories;
  }

  @override
  set trendingStories(List<Article> value) {
    _$trendingStoriesAtom.context.conditionallyRunInAction(() {
      super.trendingStories = value;
      _$trendingStoriesAtom.reportChanged();
    }, _$trendingStoriesAtom, name: '${_$trendingStoriesAtom.name}_set');
  }

  final _$getTrendingRepositoriesAsyncAction =
      AsyncAction('getTrendingRepositories');

  @override
  Future getTrendingRepositories() {
    return _$getTrendingRepositoriesAsyncAction
        .run(() => super.getTrendingRepositories());
  }

  @override
  String toString() {
    final string = 'trendingStories: ${trendingStories.toString()}';
    return '{$string}';
  }
}
