// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:newsapi/newsapi.dart';
import 'package:newsapi/src/screens/techcrunch_trending_screeh/widgets/techcrunch_trending_item.dart';
import 'package:provider/provider.dart';

class TechCrunchTrendingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TechCrunchTrendingScreenState();
  }
}

class _TechCrunchTrendingScreenState extends State<TechCrunchTrendingScreen> with AutomaticKeepAliveClientMixin {
  TechCrunchTrendingStore _techCrunchTrendingStore;

  @override
  void dispose() {
    _techCrunchTrendingStore.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _techCrunchTrendingStore = Provider.of<TechCrunchTrendingStore>(context);
    _techCrunchTrendingStore.getTrendingRepositories();

    return Center(
      child: Container(
        child: Observer(builder: (context) {
          final _trendingStories = _techCrunchTrendingStore.trendingStories;
          if (_trendingStories != null && _trendingStories.isNotEmpty) {
            return ListView.builder(
              itemCount: _techCrunchTrendingStore.trendingStories.length,
              itemBuilder: (context, index) {
                return TechCrunchTrendingItem(
                  article: _trendingStories[index],
                );
              },
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        }),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
