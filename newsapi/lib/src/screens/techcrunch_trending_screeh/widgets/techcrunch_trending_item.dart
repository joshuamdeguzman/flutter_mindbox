// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:newsapi/src/models/article.dart';

class TechCrunchTrendingItem extends StatelessWidget {
  const TechCrunchTrendingItem({Key key, this.article}) : super(key: key);
  final Article article;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: CachedNetworkImage(imageUrl: article.urlToImage),
            ),
            Text(
              article.title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text("by ${article.author}"),
          ],
        ),
      ),
    );
  }
}
