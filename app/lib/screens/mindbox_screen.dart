// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:hackernews/hackernews.dart';
import 'package:newsapi/newsapi.dart';
import 'package:redux/redux.dart';
import 'package:typicode/typicode.dart';
import 'package:provider/provider.dart';

class MindBoxScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    final Client client = Provider.of<Client>(context);

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(FontAwesomeIcons.hackerNews)),
              Tab(icon: Icon(FontAwesomeIcons.newspaper)),
              Tab(icon: Icon(Icons.group)),
            ],
          ),
          title: Text('Flutter Mindbox'),
        ),
        body: TabBarView(
          children: [
            // Bloc
            MultiBlocProvider(
              providers: [
                BlocProvider<TopStoriesBloc>(
                  create: (_) => TopStoriesBloc(
                    topStoriesRepository: TopStoriesRepository(
                      HackernewsApi(
                        client,
                        "https://hacker-news.firebaseio.com/v0",
                      ),
                    ),
                  ),
                ),
              ],
              child: HackerNewsTopStories(),
            ),
            // Mobx
            MultiProvider(
              providers: [
                Provider(
                  create: (_) => TechCrunchTrendingStore(
                    TechCrunchTrendingRepository(
                      NewsapiApi(
                        client,
                        "http://newsapi.org/v2",
                        DotEnv().env['NEWSAPI_KEY'],
                      ),
                    ),
                  ),
                ),
              ],
              child: TechCrunchTrendingScreen(),
            ),
            // Redux
            StoreProvider<FetchUsersState>(
              store: Store<FetchUsersState>(
                fetchUsersReducers,
                initialState: FetchUsersInitial(),
                middleware: [
                  FetchUsersMiddleware(
                    FetchUsersRepository(
                      TypicodeApi(
                        client,
                        "https://jsonplaceholder.typicode.com",
                      ),
                    ),
                  ),
                ],
              ),
              child: FetchUsersScreen(),
            ),
          ],
        ),
      ),
    );
  }
}
