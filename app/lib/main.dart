import 'package:app/screens/mindbox_screen.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:hackernews/hackernews.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

Future main() async {
  await DotEnv().load('.env');

  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MindBox());
}

class MindBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (_) => Client()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MindBoxScreen(),
      ),
    );
  }
}
