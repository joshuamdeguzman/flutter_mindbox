// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:typicode/src/models/typicode_models.dart';

abstract class ITypicodeApi {
  ITypicodeApi(this.client, this.baseUrl);

  final http.Client client;
  final String baseUrl;

  Future<List<User>> fetchUsers();

  void dispose();
}

class TypicodeApi extends ITypicodeApi {
  TypicodeApi(http.Client client, String baseUrl) : super(client, baseUrl);

  @override
  Future<List<User>> fetchUsers() async {
    var response = await client.get('$baseUrl/users');

    if (response.statusCode == 200) {
      final usersJson = json.decode(response.body);
      List<User> users = [];
      usersJson.forEach((v) {
        users.add(User.fromJson(v));
      });
      return users;
    } else if (response.statusCode == 999) {
      // TODO: Add other necessary status codes here depending on the API you are consuming.
      throw UnimplementedError("Add implementation here!");
    } else {
      throw Exception('Failed to fetch test users');
    }
  }

  @override
  void dispose() {
    client.close();
  }
}
