// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:typicode/src/screens/fetch_users/viewmodel/fetch_users_viewmodel.dart';
import 'package:typicode/src/screens/fetch_users/widgets/fetch_users_item.dart';
import 'package:typicode/typicode.dart';

class FetchUsersScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FetchUsersScreenState();
  }
}

class _FetchUsersScreenState extends State<FetchUsersScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: StoreConnector<FetchUsersState, FetchUsersViewModel>(
        builder: (BuildContext context, FetchUsersViewModel viewModel) {
          if (viewModel.state is FetchUsersLoading) {
            return CircularProgressIndicator();
          } else if (viewModel.state is FetchUsersEmpty) {
            return Text("Fetch users empty");
          } else if (viewModel.state is FetchUsersInitial) {
            viewModel.fetchUsers();
            return CircularProgressIndicator();
          } else if (viewModel.state is FetchUsersError) {
            return Text("Error fetching users");
          } else if (viewModel.state is FetchUsersLoaded) {
            final state = viewModel.state as FetchUsersLoaded;
            return ListView.builder(
              itemCount: state.users.length,
              itemBuilder: (context, index) {
                return FetchUsersItem(
                  user: state.users[index],
                );
              },
            );
          }

          throw UnsupportedError("Unsupported redux state!");
        },
        converter: (store) {
          return FetchUsersViewModel(
            store: store,
            state: store.state,
          );
        },
      ),
    );
  }
}
