// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:redux/redux.dart';
import 'package:typicode/typicode.dart';

class FetchUsersViewModel {
  final Store store;
  final FetchUsersState state;

  FetchUsersViewModel({this.store, this.state});

  void fetchUsers() {
    store.dispatch(FetchAction());
  }
}
