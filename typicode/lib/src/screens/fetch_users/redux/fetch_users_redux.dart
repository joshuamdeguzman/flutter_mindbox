// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:async/async.dart';
import 'package:redux/redux.dart';
import 'package:typicode/src/models/typicode_models.dart';
import 'package:typicode/src/repositories/fetch_users_repository.dart';

// States
abstract class FetchUsersState {}

class FetchUsersInitial implements FetchUsersState {}

class FetchUsersLoading implements FetchUsersState {}

class FetchUsersEmpty implements FetchUsersState {}

class FetchUsersLoaded implements FetchUsersState {
  final List<User> users;

  FetchUsersLoaded(this.users);
}

class FetchUsersError implements FetchUsersState {}

// Actions
class FetchAction {}

class FetchLoadingAction {}

class FetchErrorAction {}

class FetchResultAction {
  final List<User> users;

  FetchResultAction(this.users);
}

// Reducers
final fetchUsersReducers = combineReducers<FetchUsersState>([
  TypedReducer<FetchUsersState, FetchLoadingAction>(_onLoad),
  TypedReducer<FetchUsersState, FetchErrorAction>(_onError),
  TypedReducer<FetchUsersState, FetchResultAction>(_onResult),
]);

FetchUsersState _onLoad(FetchUsersState state, FetchLoadingAction action) =>
    FetchUsersLoading();

FetchUsersState _onError(FetchUsersState state, FetchErrorAction action) =>
    FetchUsersError();

FetchUsersState _onResult(FetchUsersState state, FetchResultAction action) =>
    action.users.isEmpty ? FetchUsersEmpty() : FetchUsersLoaded(action.users);

// Middleware
class FetchUsersMiddleware implements MiddlewareClass<FetchUsersState> {
  FetchUsersMiddleware(this.fetchUsersRepository);
  final FetchUsersRepository fetchUsersRepository;

  CancelableOperation<Store<FetchUsersState>> _operation;

  @override
  void call(Store<FetchUsersState> store, dynamic action, NextDispatcher next) {
    if (action is FetchAction) {
      store.dispatch(FetchLoadingAction());
      _operation?.cancel();
      _operation =
          CancelableOperation.fromFuture(fetchUsersRepository.fetchUsers())
              .then((result) => store
                ..dispatch(FetchResultAction(result))
                    .catchError((e, s) => store..dispatch(FetchErrorAction())));
    }

    // Notify middle aware about the actions
    next(action);
  }
}
