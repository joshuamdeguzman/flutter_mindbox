// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:typicode/src/api/typicode_api.dart';
import 'package:typicode/src/models/typicode_models.dart';

abstract class IFetchUsersRepository {
  IFetchUsersRepository(this.typicodeApi);
  final TypicodeApi typicodeApi;

  Future<List<User>> fetchUsers();

  void dispose();
}

class FetchUsersRepository extends IFetchUsersRepository {
  FetchUsersRepository(TypicodeApi typicodeApi) : super(typicodeApi);

  @override
  Future<List<User>> fetchUsers() {
    return typicodeApi.fetchUsers();
  }

  void dispose() {
    typicodeApi.dispose();
  }
}
