// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hackernews/hackernews.dart';
import 'package:hackernews/src/screens/top_stories/bloc/top_stories_event.dart';
import 'package:hackernews/src/screens/top_stories/bloc/top_stories_state.dart';
import 'package:mockito/mockito.dart';

class MockTopStoriesBloc extends MockBloc<TopStoriesEvent, TopStoriesState>
    implements TopStoriesBloc {}

class MockTopStoriesRepository extends Mock implements ITopStoriesRepository {}

void main() {
  group('TopStoriesBloc', () {
    var topStoriesRepository = MockTopStoriesRepository();

    blocTest(
      'emits[] when no event is invoked',
      build: () async => TopStoriesBloc(
        topStoriesRepository: topStoriesRepository,
      ),
      expect: [],
    );

    blocTest(
      'emits[n] of stories when LoadTopStories event is invoked',
      build: () async => TopStoriesBloc(
        topStoriesRepository: topStoriesRepository,
      ),
      act: (bloc) => bloc.add(LoadTopStories()),
      expect: [
        TopStoriesStateLoading(),
        TopStoriesStateLoaded(),
      ],
      verify: (bloc) async {
        verify(topStoriesRepository.getTopStories()).called(1);
      },
    );
  });
}
