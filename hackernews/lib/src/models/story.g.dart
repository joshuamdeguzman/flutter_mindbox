// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'story.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Story _$StoryFromJson(Map<String, dynamic> json) {
  return Story(
    json['id'] as int,
    json['by'] as String,
    json['title'] as String,
    json['type'] as String,
    json['link'] as String,
    json['score'] as int,
  );
}

Map<String, dynamic> _$StoryToJson(Story instance) => <String, dynamic>{
      'id': instance.id,
      'by': instance.by,
      'title': instance.title,
      'type': instance.type,
      'link': instance.link,
      'score': instance.score,
    };
