// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:json_annotation/json_annotation.dart';

// Expected generated class for this model.
// Can be included this before generating the class serializer.
part 'story.g.dart';

@JsonSerializable(nullable: false)
class Story {
  final int id;
  final String by;
  final String title;
  final String type;
  final String link;
  final int score;

  Story(this.id, this.by, this.title, this.type, this.link, this.score);

  // Expected generated classes for this model.
  // Can be included this before generating the class serializer.
  factory Story.fromJson(Map<String, dynamic> json) => _$StoryFromJson(json);
  Map<String, dynamic> toJson() => _$StoryToJson(this);
}
