// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'dart:convert';

import 'package:hackernews/src/models/story.dart';
import 'package:http/http.dart' as http;

abstract class IHackernewsApi {
  final http.Client client;
  final String baseUrl;

  IHackernewsApi(this.client, this.baseUrl);

  Future<Story> getStory(int id);

  Future<List<dynamic>> getTopStories();

  void dispose();
}

class HackernewsApi extends IHackernewsApi {
  HackernewsApi(http.Client client, String baseUrl) : super(client, baseUrl);

  @override
  Future<Story> getStory(int id) async {
    var response = await client.get("$baseUrl/item/$id.json?print=pretty");
    if (response.statusCode == 200) {
      return Story.fromJson(json.decode(response.body));
    } else if (response.statusCode == 999) {
      // TODO: Add other necessary status codes here depending on the API you are consuming.
      throw UnimplementedError("Add implementation here!");
    } else {
      throw Exception('Failed to load top stories');
    }
  }

  @override
  Future<List<dynamic>> getTopStories() async {
    var response = await client.get("$baseUrl/topstories.json?print=pretty");
    if (response.statusCode == 200) {
      List<dynamic> topStoryIds = json.decode(response.body);
      return topStoryIds;
    } else if (response.statusCode == 999) {
      // TODO: Add other necessary status codes here depending on the API you are consuming.
      throw UnimplementedError("Add implementation here!");
    } else {
      throw Exception('Failed to load top stories');
    }
  }

  @override
  void dispose() {
    client.close();
  }
}
