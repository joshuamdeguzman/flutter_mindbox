// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:hackernews/src/api/hackernews_api.dart';
import 'package:hackernews/src/models/story.dart';

abstract class ITopStoriesRepository {
  final HackernewsApi hackernewsApi;

  ITopStoriesRepository(this.hackernewsApi);

  Future<Story> getStory(int id);

  Future<List<Story>> getTopStories();
}

class TopStoriesRepository extends ITopStoriesRepository {
  TopStoriesRepository(HackernewsApi hackernewsApi) : super(hackernewsApi);

  @override
  Future<Story> getStory(int id) {
    return hackernewsApi.getStory(id);
  }

  @override
  Future<List<Story>> getTopStories() async {
    List<dynamic> topStoryIds = await hackernewsApi.getTopStories();

    // TODO: Implement pagination
    if (topStoryIds.length >= 30) {
      List<Story> topStories = await Future.wait(
        topStoryIds.take(30).map((id) => hackernewsApi.getStory(id)),
      );
      return topStories;
    }
    return [];
  }
}
