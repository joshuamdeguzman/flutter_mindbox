// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:equatable/equatable.dart';
import 'package:hackernews/src/models/story.dart';

abstract class TopStoriesState extends Equatable {
  const TopStoriesState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class TopStoriesStateNotLoaded extends TopStoriesState {
  const TopStoriesStateNotLoaded();
}

class TopStoriesStateLoadingFailed extends TopStoriesState {
  final Exception exception;

  const TopStoriesStateLoadingFailed([this.exception]);

  @override
  List<Object> get props => [exception];
}

class TopStoriesStateLoading extends TopStoriesState {
  const TopStoriesStateLoading();
}

class TopStoriesStateLoaded extends TopStoriesState {
  final List<Story> topStories;

  const TopStoriesStateLoaded([this.topStories]);

  @override
  List<Object> get props => [topStories];
}
