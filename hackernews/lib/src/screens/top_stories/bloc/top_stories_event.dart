// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:equatable/equatable.dart';

abstract class TopStoriesEvent extends Equatable {
  const TopStoriesEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class LoadTopStories extends TopStoriesEvent {}
