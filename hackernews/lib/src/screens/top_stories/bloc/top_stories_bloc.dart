// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'dart:developer' as developer;
import 'package:hackernews/src/models/story.dart';
import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hackernews/src/repositories/top_stories_repository.dart';
import 'package:hackernews/src/screens/top_stories/bloc/top_stories_event.dart';
import 'package:hackernews/src/screens/top_stories/bloc/top_stories_state.dart';

class TopStoriesBloc extends Bloc<TopStoriesEvent, TopStoriesState> {
  final ITopStoriesRepository topStoriesRepository;

  TopStoriesBloc({
    @required this.topStoriesRepository,
  });

  @override
  TopStoriesState get initialState => TopStoriesStateNotLoaded();

  @override
  Stream<TopStoriesState> mapEventToState(TopStoriesEvent event) async* {
    yield TopStoriesStateLoading();

    try {
      final List<Story> topStories = await topStoriesRepository.getTopStories();
      yield TopStoriesStateLoaded(topStories);
    } on Exception catch (exception) {
      developer.log(
        'Could not load top stories.',
        error: exception,
      );
      yield TopStoriesStateLoadingFailed(exception);
    }
  }
}
