// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:hackernews/src/models/story.dart';

class TopStoryItem extends StatelessWidget {
  const TopStoryItem({Key key, this.story}) : super(key: key);
  final Story story;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              story.title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text("by ${story.by}"),
          ],
        ),
      ),
    );
  }
}
