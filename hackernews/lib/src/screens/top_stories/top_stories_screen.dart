// Copyright 2020 Joshua de Guzman (https://joshuadeguzman.github.io). All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hackernews/hackernews.dart';
import 'package:hackernews/src/models/story.dart';
import 'package:hackernews/src/screens/top_stories/bloc/top_stories_event.dart';
import 'package:hackernews/src/screens/top_stories/bloc/top_stories_state.dart';
import 'package:hackernews/src/screens/top_stories/bloc/widgets/top_story_item.dart';

class HackerNewsTopStories extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HackerNewsTopStoriesState();
  }
}

class _HackerNewsTopStoriesState extends State<HackerNewsTopStories> with AutomaticKeepAliveClientMixin {
  TopStoriesBloc topStoriesBloc;

  @override
  void dispose() {
    topStoriesBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    topStoriesBloc = BlocProvider.of<TopStoriesBloc>(context);

    return Center(
      child: BlocBuilder<TopStoriesBloc, TopStoriesState>(
        builder: (context, state) {
          if (state is TopStoriesStateNotLoaded) {
            topStoriesBloc.add(LoadTopStories());

            return Center(
              child: Text("No stories loaded"),
            );
          } else if (state is TopStoriesStateLoading) {
            return CircularProgressIndicator();
          } else if (state is TopStoriesStateLoadingFailed) {
            return Center(
              child: Text("No stories loaded with error ${state.exception}"),
            );
          } else if (state is TopStoriesStateLoaded) {
            List<Story> stories = state.topStories;
            return ListView.builder(
              itemCount: state.topStories.length,
              itemBuilder: (context, index) {
                return TopStoryItem(
                  story: stories[index],
                );
              },
            );
          } else {
            throw UnsupportedError("Unsupported state!");
          }
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
